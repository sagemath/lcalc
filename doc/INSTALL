To install:

0. If you are reading this and suspect that you may not know what you
   are doing, stop and check to see if an lcalc package is available
   for your Linux distribution. The easiest way to install lcalc
   "correctly" is to let someone else do it for you. Likewise if you
   use Homebrew, Conda, or Nix to manage your software packages.

   Otherwise, if you think that you want to configure, compile, test,
   and install lcalc yourself... proceed.


1. Download and decompress the tarball.

   If you're downloading version 2.0.0, for example, the tarball
   should be called lcalc-2.0.0.tar.xz.  It can be extracted on the
   command line using tar:

     $ tar -xJf lcalc-2.0.0.tar.gz

   This will create a new directory name "lcalc-2.0.0" containing the
   source code for the project. If, on the other hand, you are using a
   graphical file manager (on, say, Windows or OSX), you can most
   likely extract the tarball simply by clicking on it.


2) Configure the build.

   Lcalc comes with a "configure" script that is used to... well, to
   configure it. Run,

     $ ./configure --help

   in the project's directory to obtain a summary of its usage. The
   design of every "configure" script is more or less the same. We
   have included a documentation file called "INSTALL.automake" that
   contains generic instructions for the script. If they're confusing,
   you probably just want to use the default set of options by running

     $ ./configure

   with no arguments. Below we summarize the few lcalc-specific options
   that the "configure" script takes:

     * --with-pari

       Adds support for elliptic-curve L-functions using the PARI library.
       Requires that PARI be installed on your system in a location that
       your compiler and linker know about.

     * --enable-precision=<type>

       Specify how much precision you want in your floating-point numbers.
       The valid values for <type> are "double", "double-double",
       "quad-double", and "multiple". The default is "double".

       Both double-double and quad-double precisions require the David H.
       Bailey's "qd" library <https://www.davidhbailey.com/dhbsoftware/>.
       The "multiple" precision requires MPFR <https://www.mpfr.org/>.

     * --enable-openmp

       This experimental option compiles everything with support for
       multithreading using OpenMP <https://www.openmp.org/>.

   The "configure" script will check that your system has all of the
   dependencies required to built lcalc. If any are missing, it will
   inform you; otherwise, you're ready to build lcalc and libLfunction.


3. Compiling lcalc and libLfunction

   This step is easy. Run,

     $ make

   in the top-level project directory.


4. Testing (optional)

   Lcalc comes with a test suite for both the "lcalc" program and the
   libLfunction library. To run the test suite, run

     $ make check

   in the top-level project directory after you've successfully built
   the project (as in the previous step). Ideally you will see a
   series of PASS results. If you see any FAIL results, please let
   us know.


5) Installation

   Now that the project is builtand you've (maybe) tested the result,
   you're ready to install it. Again, this is easy if you've made the
   right choices during the configuration step. Run,

     $ make install

   and everything should be installed to the locations determined by
   the "configure" script. Note that, by default, this will try to
   install files to e.g. /usr/local and will require root privileges.
   If you want to install lcalc for only your user account, go back to
   the configuration step and select a --prefix that is writable by
   your user.

   This will install the "lcalc" program to your chosen --bindir,
   the libLfunction library to your --libdir, and its headers  to
   your --includedir.


6. Uninstallation

   The installed project files can also be removed automatically, so
   long as you keep the (unmodified) source tree around. At any later
   time, you can run

     $ make uninstall

   to remove the files that were installed by "make install". If root
   privileges were required during installation, they will also be
   required for uninstallation.
