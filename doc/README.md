Lcalc is a package for working with L-functions.

The package has two components:

1.  **libLfunction**, a C++ library containing the `L_function` class.

    Its class methods constitute the API of the package, and its
    definition can be found in the public header `L.h`.

    Correct usage of libLfunction may be gleaned from the comments in
    `L.h`, from the source code to the lcalc program in `src/lcalc`, or
    from the example program in `doc/examples/example.cc`.

2.  **lcalc**, an executable program that uses libLfunction to perform
    some common tasks.

    At the moment, lcalc can compute both the zeros and values of
    several families of L-functions. The default L-function that lcalc
    uses is the Riemann zeta function.  To compute its first ten
    zeros, for example, you can run

    `$ lcalc --zeros 10`

    To compute its value at the point `x + yi = 0.5 + 100i`, you would
    instead run

    `$ lcalc --value --x=0.5 --y=100`

    More examples can be found in the `lcalc(1)` man page, and a usage
    summary can be obtained by running `lcalc --help`.
