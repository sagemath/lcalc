#!/bin/sh
#
# Verify the zeros of the Ramanujan tau L-function found using lcalc's
# built-in "--tau" flag against the values found when the data for the
# tau L-function are loaded from the data in the "examples" directory.
#
EXPECTED=$("${lcalc}" -F "${example_data}/data_tau" -z 9)
ACTUAL=$("${lcalc}" --tau -z 9)

printf "Testing the output of lcalc --tau -z 9... "

. "${testlib}/compare_fp_lists.sh"
compare_fp_lists "${EXPECTED}" "${ACTUAL}"
