// Test that the find_zeros() method can be passed a pointer to a
// result vector that will then be filled with the zeros found. This
// is how SageMath obtains zeros.
#include "L.h"

int main (int argc, char *argv[]) {
    initialize_globals();

    // Obtained from "lcalc --zeros 10"
    Double expected[] = {
      14.134725141738,
      21.02203963877,
      25.010857580141,
      30.424876125858,
      32.93506158774,
      37.586178158826,
      40.918719012148,
      43.327073280916,
      48.005150881167,
      49.773832477671
    };

    vector<Double> zeros;
    L_function<int> zeta;
    zeta.find_zeros(10, 0, 1025, -1, "", &zeros);

    // Default the overall result to success, and change it to failure
    // if any of the results are off by too much.
    int result = 0;
    Double error = 0;
    for (unsigned int i = 0; i < zeros.size(); i++) {
      error = abs(zeros[i] - expected[i]);
      cout << "error[" << i << "]: " << error << "... ";
      if (error < 1e-6) {
	cout << "PASS";
      }
      else {
	result = 1;
	cout << "FAIL";
      }
      cout << endl;
    }


    return result;
}
